/* =============================================================================
 * Util
 * ========================================================================== */
var ginowanUtil = function(){
    
    // ----------------------------------------------------
    /**
     * GET
     */
    this.get = function( url, data, type, success ){
        var ret = false;
        $.ajax({
            'url': url,
            'data': data,
            'type': 'get',
            'dataType': type,
            'error': function(e){
                alert('エラー');
            },
            'success': success
        });
    };
    // ----------------------------------------------------
    /**
     * ポスト
     */
    this.post = function( url, data, type, success ){
        var ret = false;
        $.ajax({
            'url': url,
            'data': data,
            'type': 'post',
            'dataType': type,
            'error': function(e){
                alert('エラー');
            },
            'success': success
        });
    }
    
    // ----------------------------------------------------
    
};