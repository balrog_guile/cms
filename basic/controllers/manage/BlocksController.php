<?php

namespace app\controllers\manage;

use Yii;
use app\models\BlocksModel;
use app\models\BlocksSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\QueryBuilder;
use yii\db\Schema;

/**
 * BlocksController implements the CRUD actions for BlocksModel model.
 */
class BlocksController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    // ----------------------------------------------------
    /**
     * 管理トップ
     * Lists all BlocksModel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlocksSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        //ビューデータ
        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'messageList' => [
                'success' => Yii::$app->session->getFlash('successMessage'),
                'error' => Yii::$app->session->getFlash('errorMessage'),
            ]
        ];
        
        //判定用
        $alReady = [];
        foreach(
            BlocksModel::find()->select('class_name')->all()
            as
            $row
        ){
            $alReady[] = $row->class_name;
        }
        
        
        //未インストールブロックを探す
        $path = Yii::getAlias('@app') . '/blocks/*';
        $data['noInstall'] = [];
        foreach( glob($path, GLOB_ONLYDIR) as $dir )
        {
            $pathInfo = pathinfo( $dir );
            if(in_array( $pathInfo['basename'] . 'Blocks', $alReady))
            {
                continue;
            }
            
            $set = [
                'dir' => Yii::getAlias('@app') . '/blocks/' . $pathInfo['basename'],
                'url' => \yii\helpers\Url::home(true) . '/blocks/' . $pathInfo['basename'],
                'class' => $pathInfo['basename'] . 'Blocks',
                'classFile' => $pathInfo['basename'] . 'Blocks.php',
                'name' => '',
                'desc' => '',
            ];
            $nameSpace = 'app\\blocks\\' . $pathInfo['basename'] . '\\' . $set['class'];
            $set['name'] = $nameSpace::$blockName;
            $set['desc'] = $nameSpace::$blockDesc;
            
            
            $data['noInstall'][] = $set;
        }
        
        
        
        
        return $this->render(
                'index', 
                $data
        );
    }
    
    // ----------------------------------------------------
    /**
     * インストール実行
     */
    public function actionInstall( $targetClass )
    {
        //インストールの実行
        $namespace = 'app\\blocks\\'
                    . preg_replace( '/Blocks$/', '', $targetClass )
                    . '\\' . $targetClass;
        
        //並び順用
        $rank = BlocksModel::find()->max('rank');
        if(is_null($rank))
        {
            $rank = 1;
        }
        else{
            $rank ++;
        }
        
        //モデル空間など
        $model_class = $targetClass .'Model';
        $model_name_space = 'app\\blocks\\'
                    . preg_replace( '/Blocks$/', '', $targetClass )
                    . '\\' . $targetClass . 'Model';
        
        
        //テーブル作成
        $tableName = $model_name_space::tableName();
        $column = $model_name_space::defineColumn();
        $queryBuilder = Yii::$app->db->queryBuilder;
        $sql = $queryBuilder->createTable($tableName, $column);
        Yii::$app->db->createCommand($sql)->execute();
        
        
        
        //インストール済みに
        $model = new BlocksModel();
        $model->name = $namespace::$blockName;
        $model->desc = $namespace::$blockName;
        $model->width = $namespace::$width;
        $model->height = $namespace::$height;
        $model->create_date = date('Y-m-d H:i:s');
        $model->update_date = date('Y-m-d H:i:s');
        $model->rank = $rank;
        $model->name_sapace = $namespace;
        $model->class_name = $targetClass;
        $model->model_name_space = $model_name_space;
        $model->model_class = $model_class;
        $model->save();
        
        
        Yii::$app->session->setFlash( 'successMessage', 'ブロックを登録しました');
        
        $this->redirect(['index']);
    }
    
    // ----------------------------------------------------
    
    /**
     * Displays a single BlocksModel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    
    // ----------------------------------------------------
    /**
     * Creates a new BlocksModel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BlocksModel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BlocksModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BlocksModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BlocksModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BlocksModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BlocksModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
