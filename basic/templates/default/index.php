<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>サンプル１５ | デザインなう</title>
<meta name="keywords" content="キーワード" />
<meta name="description" content="サイトの説明" />
<meta http-equiv="Content-Language" content="ja" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="imagetoolbar" content="no" />
<link href="share/css/set/import.css" rel="stylesheet" type="text/css" media="all" />
<link href="share/css/share.css" rel="stylesheet" type="text/css" media="all" />
<link href="index/css/index.css" rel="stylesheet" type="text/css" media="all" />
<link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
<link href="favicon.ico" rel="icon" type="image/vnd.microsoft.icon" />
<script type="text/javascript" src="share/js/default.js"></script>
</head>
<body>
	<div id="wrapper">
		<div id="header" class="clearfix">
			<h1>CREATIVE SAMPLE.ltd</h1>
			<ul id="gNavi" class="clearfix">
				<li class="firstItem"><a href="sample.html" title="">MENU 6</a></li>
				<li><a href="sample.html" title="">MENU 5</a></li>
				<li><a href="sample.html" title="">MENU 4</a></li>
				<li><a href="sample.html" title="">MENU 3</a></li>
				<li><a href="sample.html" title="">MENU 2</a></li>
				<li><a href="sample.html" title="">MENU 1</a></li>
				<li class="lastItem"><a href="index.html" title="">TOP PAGE</a></li>
			</ul>
		</div>
		<div id="mainImgBox">
			<div id="mainMessageBox">
				<strong>私たちがサポート致します。</strong>
				<span>そこは事実まるで同じ話がかりという事の時を足りたましょ。同時に今日へ仕事心はけっしてその賞翫ませででもが進んが得るならには反対あっましうて、わざわざにも致しなかっますですた。眼で投げでしのはどうも晩に毫もですうです。</span>
			</div>
		</div>
		<div id="mainContents" class="clearfix">
			<div id="outlineBox">
				<h2><span>ABOUT</span></h2>
				<dl>
					<dt>屋号</dt>
					<dd>クリエイティブサンプル株式会社</dd>
					<dt>設立</dt>
					<dd>2033年1月1日</dd>
					<dt>資本金</dt>
					<dd>1,000万円</dd>
					<dt>所在地</dt>
					<dd>東京都住所1-1-1 サンプルビル 112</dd>
					<dt>事業内容</dt>
					<dd>事業内容、事業内容、事業内容、事業内容内容、事業、事業内容</dd>
				</dl>
			</div>

			<div id="conceptBox">
				<h2><span>CONCPET</span></h2>
				<strong>見出しが入ります。</strong>
				<span>そこは事実まるで同じ話がかりという事の時を足りたましょ。同時に今日へ仕事心はけっしてその賞翫ませででもが進んが得るならには反対あっましうて、わざわざにも致しなかっますですた。眼で投げでしのはどうも晩に毫もですうです。
そこは事実まるで同じ話がかりという事の時を足りたましょ。</span>
			</div>
			<div id="topicsBox">
				<h2><span>NEW TOPICS</span></h2>
				<dl class="clearfix">
					<dt>0000.00.00</dt>
					<dd><a href="#" title="">新着情報がはいり</a></dd>
					<dt>0000.00.00</dt>
					<dd><a href="#" title="">新着情報がはいります。</a></dd>
					<dt>0000.00.00</dt>
					<dd><a href="#" title="">新着情報がはいります。</a></dd>
					<dt>0000.00.00</dt>
					<dd><a href="#" title="">新着情報がはいり</a></dd>
					<dt>0000.00.00</dt>
					<dd><a href="#" title="">新着情報がはいり新着情報が</a></dd>
				</dl>
			</div>

		</div>
		<div id="footer">
			<p id="copyright">Copyright (c) ◎◎◎◎◎◎◎ All Rights Reserved.</p>
		</div>
	</div>

</body>
</html>