<?php
/* =============================================================================
 * ブロック用のAPI
 * ========================================================================= */
namespace app\controllers\manage\api;

use app\models\PostModel;
use app\models\PostVersionModel;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use app\models\BlocksModel;
use app\models\BlocksSearchModel;

use app\blocks\BlocksAdminModel;
use app\blocks\html\htmlBlocksModel;

use app\models\VersionBlocksModel;


class BlocksController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    
    // ----------------------------------------------------
    /**
     * ブロックリスト取得
     */
    public function actionIndex( $list = null )
    {
        $data = [];
        
        $searchModel = new BlocksSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        //ビューデータ
        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'messageList' => [
                'success' => Yii::$app->session->getFlash('successMessage'),
                'error' => Yii::$app->session->getFlash('errorMessage'),
            ],
            'areaName' => Yii::$app->request->get('areaName'),
        ];
        
        
        return $this->renderPartial( 'index', $data );
    }
    
    // ----------------------------------------------------
    /**
     * 編集呼び出し
     */
    public function actionEdit()
    {
        $editingVersionId = Yii::$app->request->get('editingVersionId');
        $blockId = Yii::$app->request->get('blockId');
        $postId = Yii::$app->request->get('postId');
        $areaName = Yii::$app->request->get('areaName');
        
        //インストール済みブロックを取得
        $installedBlock = BlocksModel::find( $editingVersionId )->one();
        if( $installedBlock === null )
        {
            new NotFoundHttpException();
            return;
        }
        
        
        //対象ポストを探す
        $versionModel = PostVersionModel::find()
                ->where('id = :id', [':id' => $editingVersionId] )
                ->one();
        
        
        
        //モデルを呼び出す
        $modelName = $installedBlock->model_name_space;
        if((int)$blockId==0)//新規追加
        {
            $model = new $modelName;
            $model->loadDefaultValues();
            
            $blockVersionModel = new VersionBlocksModel();
            $blockVersionModel->loadDefaultValues();
            
        }
        else//編集
        {
            
        }
        
        
        
        //データセーブ
        if($model->load(Yii::$app->request->post()))
        {
            $blockVersionModel->load(Yii::$app->request->post());
            
            //追加
            if((int)$blockId==0)
            {
                $model->create_date = date('Y-m-d H:i:s');
                $model->update_date = date('Y-m-d H:i:s');
                $model->user_id = Yii::$app->user->id;
                
                //表示順
                $max = VersionBlocksModel::find()
                        ->where(
                            'post_version_id = :post_version_id AND area_name = :area_name',
                            [
                                ':post_version_id' => $versionModel->id,
                                ':area_name' => $areaName,
                            ]
                        )
                        ->max('rank');
                        //->one();
                $max ++ ;
                
                //バージョン記録
                $blockVersionModel->post_version_id = $versionModel->id;
                $blockVersionModel->blocks_id = $installedBlock->id;
                $blockVersionModel->area_name = $areaName;
                $blockVersionModel->create_date = date('Y-m-d H:i:s');
                $blockVersionModel->update_date = date('Y-m-d H:i:s');
                $blockVersionModel->rank = $max;
                
                
            }
            //編集時
            else
            {
                $model->post_id = $postId;
                $model->version_id = $versionModel->id;
                $model->create_date = date('Y-m-d H:i:s');
                $model->update_date = date('Y-m-d H:i:s');
                $model->user_id = Yii::$app->user->id;
                
                //バージョン記録
                //$blockVersionModel->post_version_id = $versionModel->id;
                //$blockVersionModel->blocks_id = $installedBlock->id;
                //$blockVersionModel->area_name = $areaName;
                //$blockVersionModel->create_date = date('Y-m-d H:i:s');
                $blockVersionModel->update_date = date('Y-m-d H:i:s');
                
            }
            if( $model->save() )
            {
                $blockVersionModel->block_data_id = $model->id;
                if( $blockVersionModel->save() )
                {
                    echo '{{GINOWANCODE::::BLOCK_EDIT_OK}}';
                    return;
                }
            }
        }
        
        
        $data = [
            'installedBlock' => $installedBlock,
            'model' => $model,
            'blockVersionModel' => $blockVersionModel,
        ];
        
        return $this->renderPartial( 'edit', $data );
    }
    
    // ----------------------------------------------------
    
}