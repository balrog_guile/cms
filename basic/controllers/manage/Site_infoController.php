<?php
/* =============================================================================
 * サイト情報コントローラー
 * ========================================================================== */
namespace app\controllers\manage;

use Yii;
use app\models\SiteInfoModel;
use app\models\SiteInfoSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;
use app\filters\AccessRule2;


/**
 * Site_infoController implements the CRUD actions for SiteInfoModel model.
 */
class Site_infoController extends Controller
{
    // ----------------------------------------------------
    
    public function behaviors()
    {
        return [
            //動作定義
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
            //アクセス管理
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule2::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['create','index','delete','view','update'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ]
                ],
            ],
        ];
    }
    
    // ----------------------------------------------------
    
    /**
     * Lists all SiteInfoModel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteInfoSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    // ----------------------------------------------------
    
    /**
     * Displays a single SiteInfoModel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    // ----------------------------------------------------
    
    /**
     * Creates a new SiteInfoModel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SiteInfoModel();
        
        //登録時
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        
        
        //初期値設定
        $model->is_active = 1;
        $model->mobile_mode = 0;
        $model->active_template = 'default';
        $model->active_sp_tenplate = 'default';
        
        
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    // ----------------------------------------------------
    
    /**
     * Updates an existing SiteInfoModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        //編集実行
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    // ----------------------------------------------------
    
    /**
     * Deletes an existing SiteInfoModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index']);
    }
    
    // ----------------------------------------------------
    
    /**
     * Finds the SiteInfoModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SiteInfoModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SiteInfoModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    // ----------------------------------------------------
}
