<?php
/* =============================================================================
 * ポストリスト管理画面用
 * ========================================================================== */
namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PostListModel;

/**
 * PostListSearchModel represents the model behind the search form about `app\models\PostListModel`.
 */
class PostListSearchModel extends PostListModel
{
    
    // ----------------------------------------------------
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'open_status', 'delete_flag'], 'integer'],
            [['key', 'name', 'create_date', 'update_date'], 'safe'],
        ];
    }
    
    // ----------------------------------------------------
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    // ----------------------------------------------------
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PostListModel::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'open_status' => $this->open_status,
            'delete_flag' => $this->delete_flag,
        ]);
        
        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'name', $this->name]);
        
        return $dataProvider;
    }
    
    // ----------------------------------------------------
    }
