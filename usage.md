# yiiインストール

composer global require "fxp/composer-asset-plugin:~1.0.0"
composer create-project --prefer-dist yiisoft/yii2-app-basic basic

## インストール後スべきこと
- db.php設定（ローカルで実行する場合は、unix_socketも設定）  
- .htaccess設置
- web.phpにURLmanagerの設定

---

# yii2-user

https://github.com/dektrium/yii2-user  

## install
https://github.com/dektrium/yii2-user/blob/master/docs/getting-started.md  
**basicディレクトリ**で実行

## RBAC
https://github.com/dektrium/yii2-user/blob/master/docs/custom-access-control.md  
を参考にすれば良いが、AccessRuleの名前空間が処理できないので、  
クラス名をAccessRule2など別名にする。

---
# gii
http://domain/gii
でアクセス。モデルの作成は変わらず。

## CRUD
- Model: app/models/モデル名
- SearchModel: app/models/モデル名Serchなど
- Controller: app/contollers/コントローラ名Controller
- ビュー: @app/views/コントローラー名（実存在する箇所を指定らしい）