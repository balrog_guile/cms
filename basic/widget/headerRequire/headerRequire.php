<?php
/* =============================================================================
 * ヘッダ
 * ========================================================================== */
namespace app\widget\headerRequire;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\base\Widget;

class headerRequire extends Widget
{
    
    // ----------------------------------------------------
    /**
     * 共通処理
     */
    public function commonSet( &$data )
    {
        
    }
    // ----------------------------------------------------
    
    public function init()
    {
        parent::init();
        
    }
    
    // ----------------------------------------------------
    public function run()
    {
        $data = Yii::$app->controller->viewData;
        $this->commonSet($data);
        echo $this->render('header',$data);
    }
    // ----------------------------------------------------
}