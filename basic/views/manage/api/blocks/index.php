<ul class="gnwn_listPanel row">
    <?php foreach( $dataProvider->getModels() as $model ): ?>
    <?php $block = app\helpers\Cms::getBlockData($model); ?>
    <?php //var_dump($block); ?>
    <li class="w10 col-md-3">
        <div class="margin08">
            <a
                href="javascript:void(0);"
                class="gnwn_listPanel_editBlock"
                data-blockInstallId="<?= $model->id; ?>"
                data-blockPanelWidth="<?= $model->width; ?>"
                data-blockPanelHeight="<?= $model->height; ?>"
                data-blockName="<?= $model->name; ?>"
                data-area-name="<?= $areaName; ?>"
            >
                <img src="<?= $block->url; ?>icon.png" />
            </a>
        </div>
        <p class="name">
            <?= $model->name; ?>
        </p>
        <p class="detail">
            <?= $model->desc; ?>
        </p>
    </li>
    <?php endforeach;?>
</ul>