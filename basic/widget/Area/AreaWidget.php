<?php
/* =============================================================================
 * エリア
 * ========================================================================== */
namespace app\widget\Area;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\base\Widget;
use app\models\VersionBlocksModel;

class AreaWidget extends Widget
{
    //エリア名
    public $name;
    
    //呼び出しするブロック
    public $callBlocks;
    
    //最大追加数
    public $max;
    
    //ブロック追加
    public $addBlocks = true;
    
    //outputデータ
    public $output;
    
    // ----------------------------------------------------
    /**
     * 共通処理
     */
    public function commonSet( &$data )
    {
        $data['name'] = $this->name;
    }
    // ----------------------------------------------------
    
    public function init()
    {
        parent::init();
        
        $this->output = new \stdClass();
        
        $data = Yii::$app->controller->viewData;
        $this->commonSet($data);
        echo $this->render('start',$data);
        
        //////データをアサイン
        if( Yii::$app->controller->editing->edit_now === true )
        {
            $version = Yii::$app->controller->post->editingVersion->id;
        }
        else
        {
            $version = Yii::$app->controller->post->currentVersion->id;
        }
        $data['versionId'] = $version;
        
        
        
        foreach(
            VersionBlocksModel::getAreaDataList( $version, $this->name )
            as
            $model
        )
        {
            //echo $model->blocks->model_name_space;
            $name = $model->blocks->model_name_space;
            $this->output->{$model->name} = 
                    $name::find()
                        ->where([
                                'id' => $model->block_data_id
                        ])
                        ->one();
        }
    }
    
    // ----------------------------------------------------
    
    public function run()
    {
        $data = Yii::$app->controller->viewData;
        $this->commonSet($data);
        echo $this->render('end',$data);
        if(!is_null(Yii::$app->user->id))
        {
            echo $this->render('addArea', $data );
        }
    }
    // ----------------------------------------------------
}