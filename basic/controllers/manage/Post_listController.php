<?php
/* =============================================================================
 * ポストリスト管理
 * ========================================================================== */
namespace app\controllers\manage;

use Yii;
use app\models\PostListModel;
use app\models\PostListSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use yii\filters\AccessControl;
use app\filters\AccessRule2;


use yii\filters\VerbFilter;

/**
 * Post_listController implements the CRUD actions for PostListModel model.
 */
class Post_listController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['post'],
                ],
            ],
            
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule2::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['create','index','delete','view','update'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ]
                ],
            ],
        ];
    }
    
    // ----------------------------------------------------
    
    /**
     * Lists all PostListModel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostListSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    // ----------------------------------------------------
    /**
     * Displays a single PostListModel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    // ----------------------------------------------------
    /**
     * 共通設定
     * @param string $mode
     * @param $model
     */
    protected function setCommonData( $mode, &$model )
    {
        if( $mode == 'create' )
        {
            $model->create_date = date('Y-m-d H:i:s');
            $model->update_date = date('Y-m-d H:i:s');
        }
        else if( $mode == 'update' )
        {
            $model->update_date = date('Y-m-d H:i:s');
        }
    }
    
    // ----------------------------------------------------
    
    /**
     * 作成
     */
    public function actionCreate()
    {
        $model = new PostListModel();
        
        //作成実行
        if ($model->load(Yii::$app->request->post())){
            $this->setCommonData( 'create', $model );
            
            if( $model->save() )
            {
                return $this->redirect(['index']);
            }
        }
        
        
        //初期値設定
        $model->open_status = 1;
        
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    // ----------------------------------------------------
    
    /**
     * Updates an existing PostListModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //作成実行
        if ($model->load(Yii::$app->request->post())){
            $this->setCommonData( 'update', $model );
            
            if( $model->save() )
            {
                return $this->redirect(['index']);
            }
        }
        
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PostListModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PostListModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PostListModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PostListModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
