<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%version_blocks}}".
 *
 * @property integer $id
 * @property integer $post_version_id
 * @property integer $blocks_id
 * @property string $area_name
 * @property integer $block_data_id
 * @property integer $rank
 * @property integer $is_publish
 * @property string $create_date
 * @property string $update_date
 * @property integer $delete_flag
 */
class VersionBlocksModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%version_blocks}}';
    }
    
    // ----------------------------------------------------
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_version_id', 'blocks_id', 'area_name', 'block_data_id', 'create_date', 'update_date', 'name'], 'required'],
            [['post_version_id', 'blocks_id', 'block_data_id', 'rank', 'is_publish', 'delete_flag'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['area_name'], 'string', 'max' => 255]
        ];
    }
    
    // ----------------------------------------------------
    /**
     * リレーション：ブロック
     */
    public function getBlocks()
    {
        return $this->hasOne(
            BlocksModel::className(),
            [
                'id' => 'blocks_id'
            ]
        );
    }
    
    // ----------------------------------------------------
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_version_id' => Yii::t('app', 'バージョンID'),
            'blocks_id' => Yii::t('app', 'ブロックID'),
            'area_name' => Yii::t('app', 'エリア名'),
            'name' => Yii::t('app', 'ブロック名'),
            'block_data_id' => Yii::t('app', 'Block Data ID'),
            'rank' => Yii::t('app', '表示順'),
            'is_publish' => Yii::t('app', '公開状態'),
            'create_date' => Yii::t('app', '更新日'),
            'update_date' => Yii::t('app', 'Update Date'),
            'delete_flag' => Yii::t('app', '削除フラグ'),
        ];
    }
    
    // ----------------------------------------------------
    /**
     * エリアデータ
     * @param int $versionId
     * @param string $area
     */
    static public function getAreaDataList( $versionId, $area )
    {
        return self::find()
                ->where([
                    'post_version_id' => $versionId,
                    'area_name' => $area
                ])
                ->orderBy('rank ASC')
                ->all();
    }
    
    // ----------------------------------------------------
    
    /**
     * @inheritdoc
     * @return VersionBlocksQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VersionBlocksQuery(get_called_class());
    }
    
    // ----------------------------------------------------
}
