<?php
/* =============================================================================
 * HTMLブロック
 * ========================================================================== */
namespace app\blocks\html;
use app\blocks\BlocksAdmin;
use yii\web\Controller;

/**
 * 各ブロックもウィジェットの拡張として実装する。
 * そのため、編集画面の自由度がますように設計されている。
 */

class htmlBlocks extends BlocksAdmin
{
    //ブロック名
    static public $blockName = 'HTML入力';
    //簡単な説明
    static public $blockDesc = 'HTMLを入力できるブロックです';
    //設定パネルの幅(%指定)
    static public $width = '80';
    //設定パネルの高さ(%指定)
    static public $height = '80';
    
    // ----------------------------------------------------
    
     public function init()
    {
        parent::init();
    }
    
    // ----------------------------------------------------
    
    public function run()
    {
    }
    
    // ----------------------------------------------------
}