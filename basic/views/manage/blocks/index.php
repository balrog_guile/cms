<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BlocksSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'ブロック管理');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blocks-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?php if( $messageList['success'] != '' ): ?>
        <div class="alert alert-success"><?= $messageList['success']; ?></div>
    <?php endif; ?>
    <?php if( $messageList['error'] != '' ): ?>
        <div class="alert alert-danger"><?= $messageList['success']; ?></div>
    <?php endif; ?>
    
    
    <h2>インストール済みブロック</h2>
    <div class="container">
        <ul class="gnwn_listPanel row">
            <?php foreach( $dataProvider->getModels() as $model ): ?>
            <?php $block = app\helpers\Cms::getBlockData($model); ?>
            <?php //var_dump($block); ?>
            <li class="w10 col-md-3">
                <div class="margin08">
                        <img src="<?= $block->url; ?>icon.png" />
                </div>
                <p class="name">
                    <?= $model->name; ?>
                </p>
                <p class="detail">
                    <?= $model->desc; ?>
                </p>
            </li>
            <?php endforeach;?>
        </ul>

        <?php
            echo \yii\widgets\LinkPager::widget([
                'pagination'=>$dataProvider->pagination,
            ]);
        ?>
    </div>
    
    
    <hr />
    
    <h2>未インストールブロック</h2>
        <div class="container">
        <?php if( count($noInstall) > 0 ): ?>
            <ul class="gnwn_listPanel row">
                <?php foreach( $noInstall as $nl ): ?>
                <li class="w10 col-md-3">
                    <div class="margin08">
                        <a href="<?php echo Yii::$app->urlManager->createUrl(['manage/blocks/install','targetClass' => $nl['class']]); ?>">
                            <img src="<?= $nl['url']?>/icon.png" />
                        </a>
                    </div>
                    <p class="name">
                        <?= $nl['name']; ?>
                    </p>
                    <p class="detail">
                        <?= $nl['desc']; ?>
                    </p>
                </li>
                <?php endforeach;?>
            </ul>
        <?php else: ?>
            <div class="alert alert-info">未インストールブロックはありません。</div>
        <?php endif; ?>
        
    </div>
    
</div>
