<?php
/* =============================================================================
 * ブロックコントローラー
 * ========================================================================== */
namespace app\blocks;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\base\Widget;

class BlocksAdmin extends Widget
{
    //ブロック名
    static public $blockName = 'デフォルトブロック';
    //簡単な説明
    static public $blockDesc = 'デフォルトブロック';
    //設定パネルの幅(%指定)
    static public $width = '80';
    //設定パネルの高さ(%指定)
    static public $height = '80';
    
    //動作モード
    public $mode = 'display';
    
    //表示に渡すデータ
    public $displayData = [];
    
    //モデル
    public $model;
    public $blockVersionModel;
    
    // ----------------------------------------------------
    
     public function init()
    {
        parent::init();
        $this->displayData['model'] = $this->model;
        $this->displayData['blockVersionModel'] = $this->blockVersionModel;
        switch( $this->mode )
        {
            case 'edit':
                $content = $this->render( 'edit', $this->displayData );
                break;
            
            case 'display':
            default:
                $content = $this->render( 'display', $this->displayData );
                break;
        }
        echo  $content;
    }
    
    // ----------------------------------------------------
    
    public function run()
    {
    }
    
    // ----------------------------------------------------
}