<?php
/* =============================================================================
 * サイトマネージャーAPI
 * ========================================================================= */
namespace app\controllers\manage\api;
use app\models\PostModel;
use app\models\PostVersionModel;
use Yii;
use yii\web\NotFoundHttpException;

class Site_manageController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    
    // ----------------------------------------------------
    /**
     * 記事追加
     */
    public function actionAdd_post( $post_list_id = 5, $id = null )
    {
        $data = array();
        if(is_null($id))
        {
            $model = new PostModel();
            $model->post_list_id = $post_list_id;
            $data['isCreate'] = true;
        }
        else
        {
            $model = PostModel::find()
                ->where(['id' => $id ])
                ->one();
            $data['isCreate'] = false;
        }
        $data['model'] = $model;
        
        
        $isInit = TRUE;
        
        if($model->load(Yii::$app->request->post())){
            
            $model->create_date = date('Y-m-d H:i:s');
            $model->update_date = date('Y-m-d H:i:s');
            if( $model->display_date == '' )
            {
                $model->display_date = date('Y-m-d H:i:s');
            }
            $isInit = FALSE;
            if( $model->save() )
            {
                $PostVersionModel = new PostVersionModel;
                $PostVersionModel->post_id = $model->id;
                $PostVersionModel->version_num = 1;
                $PostVersionModel->name = '初期バージョン';
                $PostVersionModel->create_date = date('Y-m-d H:i:s');
                $PostVersionModel->update_date = date('Y-m-d H:i:s');
                $PostVersionModel->is_publish = 1;
                $PostVersionModel->user_id = Yii::$app->user->id;
                $PostVersionModel->is_edit_now = 0;
                $PostVersionModel->save();
                
                
                return $this->renderPartial( 'addPostOK', $data );
            }
            //var_dump( $model->getErrors() );
        }
        
        //モデルデータの初期化
        if( $isInit )
        {
            $model->index_display = 1;
            $model->open_status = 1;
        }
        
        
        return $this->renderPartial( 'addPost', $data );
    }
    // ----------------------------------------------------
    /**
     * エディットモードに入る
     */
    public function actionGo_edit( $post_list_id, $id, $url, $go )
    {
        $model = PostModel::find()
               ->where(
                       'id = :id', [':id' => $id ])
               ->one();
        if(is_null($model))
        {
            $this->redirect( Yii::$app->urlManager->createAbsoluteUrl('/') );
            return;
        }
        
        //編集中バージョンがアレばなしに
        if(!is_null($model->editNow))
        {
            $this->redirect( $url );
            return;
        }
        
        //編集中モードを作成
        $max = PostVersionModel::find()
                ->where(
                    'post_id = :post_id',
                    [
                        ':post_id' => $model->id
                    ]
                )
                ->orderBy('version_num DESC')
                ->limit(1)
                ->one();
        
        $PostVersionModel = new PostVersionModel;
        $PostVersionModel->post_id = $model->id;
        $PostVersionModel->version_num = ($max->version_num + 1);
        $PostVersionModel->name = 'バージョン' . ($max->version_num + 1);
        $PostVersionModel->create_date = date('Y-m-d H:i:s');
        $PostVersionModel->update_date = date('Y-m-d H:i:s');
        $PostVersionModel->is_publish = 0;
        $PostVersionModel->user_id = Yii::$app->user->id;
        $PostVersionModel->is_edit_now = 1;
        $PostVersionModel->save();
        
        $this->redirect( $url );
        return;
    }
    
    // ----------------------------------------------------
    /**
     * 編集終了処理
     */
    public function actionFinish_edit()
    {
        $id = Yii::$app->request->post('id',-10000);
        $url = Yii::$app->request->post('url','/');
        $mode = Yii::$app->request->post('mode','poi');
        
        $model = PostModel::find()
               ->where(
                       'id = :id',
                        [
                            ':id' => $id
                        ]
               )
               ->one();
        if(is_null($model))
        {
            $this->redirect( Yii::$app->urlManager->createAbsoluteUrl('/') );
            return;
        }
        
        //編集中バージョンがなければ無視
        if(is_null($model->editNow))
        {
            $this->redirect( $url );
            return;
        }
        
        
        $target = $model->editNow;
        
        
        //編集破棄
        if( $mode == 'poi' )
        {
            $target->delete();
        }
        
        //編集を保留
        if( $mode == 'draft' )
        {
            $target->is_edit_now = 0;
            $target->save();
        }
        
        
        //公開
        if( $mode == 'open' )
        {
            PostVersionModel::updateAll(
                [
                    'is_publish' => 0,
                ],
                'post_id = :post_id',
                [
                    ':post_id' => $id
                ]
            );
            
            $target->is_publish = 1;
            $target->is_edit_now = 0;
            $target->save();
        }
        $this->redirect( $url );
        
    }
    
    // ----------------------------------------------------

}
