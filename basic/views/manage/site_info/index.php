<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SiteInfoSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'サイト情報');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-info-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'サイト情報作成'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    
    <table class="table table-bordered table-condensed">
        <thead>
            <tr>
                <th>ID</th>
                <th>サイト名</th>
                <th>ドメイン</th>
                <th>SP動作モード</th>
                <th>PCテンプレート</th>
                <th>SPテンプレート</th>
                <th>有効</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( app\models\SiteInfoModel::find()->all() as $row ): ?>
            <tr>
                <td><?= $row->id; ?></td>
                <td><?= $row->site_name; ?></td>
                <td><?= $row->domain; ?></td>
                <td><?= $row->mobile_mode; ?></td>
                <td><?= $row->active_template; ?></td>
                <td><?= $row->active_sp_tenplate; ?></td>
                <td><?php if($row->is_active==1): ?>有効<?php else: ?>無効<?php endif; ?></td>
                <td>
                    <a
                        href="<?= Yii::$app->urlManager->createAbsoluteUrl(['manage/site_info/update','id' => $row->id ]);?>"
                        class="btn btn-xs btn-primary"
                    >
                        詳細/編集
                    </a>
                    <a
                        href="<?= Yii::$app->urlManager->createAbsoluteUrl(['manage/site_info/delete','id' => $row->id ]);?>"
                        class="btn btn-xs btn-danger"
                    >
                        削除
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
