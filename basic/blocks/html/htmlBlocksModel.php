<?php
/* =============================================================================
 * HTMLブロック用モデル 
 * ========================================================================== */
namespace app\blocks\html;
use yii\db\QueryBuilder;
use yii\db\Schema;
use app\blocks\BlocksAdminModel;
use Yii;
//use yii\base\Model;

/**
 * ブロック専用のモデルファイルを用意して運用するスタイル。書き方は簡単。
 * ブロック専用のモデルがあるので、データ加工も楽ちん
 * 
 * ▼ここにプロパティを書いておくとラク
 *
 */



class htmlBlocksModel extends BlocksAdminModel
{
    // ----------------------------------------------------
    /**
     * テーブル名
     */
    public static function tableName()
    {
        return 'block_html';
    }
    
    // ----------------------------------------------------
    /**
     * ルール設定
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['html'], 'required'];
        return $rules;
    }
    
    // ----------------------------------------------------
    /**
     * ラベル設定
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['html'] = Yii::t('app', 'HTML');
        return $labels;
    }
    
    // ----------------------------------------------------
    /**
     * カラム名定義
     */
    static public function defineColumn()
    {
        $return = [
            'id' => 'pk',
            'html' => 'longtext',
        ];
        BlocksAdminModel::addBasicColumns($return);
        return $return;
    }
    
    // ----------------------------------------------------
}