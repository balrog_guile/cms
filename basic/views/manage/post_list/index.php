<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostListSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'ポストリスト管理');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-list-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <hr />
    
    <p>
        <?= Html::a(Yii::t('app', 'ポストリスト作成'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <hr />
    
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>対象サイト</th>
                <th>公開状態</th>
                <th>スラグ</th>
                <th>名前</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $dataProvider->getModels() as $row ): ?>
            <tr>
                <td><?= $row->id; ?></td>
                <td><?= $row->site_info_id; ?></td>
                <td>
                    <?php if($row->open_status==1): ?>
                        公開
                    <?php else: ?>
                        非公開
                    <?php endif; ?>
                </td>
                <td>
                    <?= $row->key; ?>
                </td>
                <td>
                   <?= $row->name; ?>
                </td>
                <td>
                    <?= Html::a(
                        Yii::t('app', '編集'),          
                        ['update', 'id' => $row->id],
                        ['class' => 'btn btn-success btn-xs']
                    )?>
                    <?= Html::a(
                        Yii::t('app', '削除'),          
                        ['delete', 'id' => $row->id],
                        ['class' => 'btn btn-danger btn-xs']
                    )?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <hr />
    
    <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination'=>$dataProvider->pagination,
        ]);
    ?>
    
    
    
    <?php /*GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'key',
            'name',
            'create_date',
            'update_date',
            // 'open_status',
            // 'delete_flag',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */
    ?>

</div>
