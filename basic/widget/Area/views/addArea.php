<?php if( $self->editing->edit_now ): ?>
    <div class="__gnwn_area_add">
        <a
            href="javscript:void(0);"
            class="__gnwn_area_add_exec"
            date-areaname="<?= $name; ?>"
        >
            <?= $name; ?>に追加
        </a>
    </div>
<?php endif; ?>
