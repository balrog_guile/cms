<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$name = $installedBlock->name_sapace;
$modelName = $installedBlock->model_name_space;
?>
<div class="gnwn__edit_block_now">
    <?php
        $name::begin([
            'mode' => 'edit',
            'model' => $model,
            'blockVersionModel' => $blockVersionModel,
        ]);
    ?>
    <?php
        $name::end();
    ?>
</div>