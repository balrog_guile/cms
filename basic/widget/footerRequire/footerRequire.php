<?php
/* =============================================================================
 * ヘッダ
 * ========================================================================== */
namespace app\widget\footerRequire;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\base\Widget;

class footerRequire extends Widget
{
    
    // ----------------------------------------------------
    /**
     * 共通処理
     */
    public function commonSet( &$data )
    {
    }
    // ----------------------------------------------------
    
    public function init()
    {
        parent::init();
        
    }
    
    // ----------------------------------------------------
    public function run()
    {
        $data = Yii::$app->controller->viewData;
        $this->commonSet($data);
        
        if(!is_null(Yii::$app->user->id))
        {
            echo $this->render('panel',$data);
        }
        
        echo $this->render('foot',$data);
    }
    // ----------------------------------------------------
}