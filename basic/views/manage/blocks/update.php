<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BlocksModel */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Blocks Model',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blocks Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="blocks-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
