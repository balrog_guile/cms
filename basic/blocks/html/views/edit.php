<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin([]); ?>
    
    <!-- 必須項目 -->
    <?= $form->field( $blockVersionModel, 'name' )->textInput(['style' => 'width: 100%;']); ?>
    <?= $form->field( $blockVersionModel, 'is_publish')->dropDownList([0 => '非公開', 1 => '公開']); ?>
    <!-- /必須項目 -->
    
    <hr />
    
    <!-- 追加フィールドを設定 -->
    <?= $form->field( $model, 'html' )->textarea(['style' => 'width: 100%; height: 150px;']); ?>
    <!-- /追加フィールドを設定 -->
    
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '追加') : Yii::t('app', '編集'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>