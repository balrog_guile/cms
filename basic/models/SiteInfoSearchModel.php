<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SiteInfoModel;

/**
 * SiteInfoSearchModel represents the model behind the search form about `app\models\SiteInfoModel`.
 */
class SiteInfoSearchModel extends SiteInfoModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mobile_mode', 'is_active'], 'integer'],
            [['site_name', 'domain', 'active_template', 'active_sp_tenplate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteInfoModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'mobile_mode' => $this->mobile_mode,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'site_name', $this->site_name])
            ->andFilterWhere(['like', 'domain', $this->domain])
            ->andFilterWhere(['like', 'active_template', $this->active_template])
            ->andFilterWhere(['like', 'active_sp_tenplate', $this->active_sp_tenplate]);

        return $dataProvider;
    }
}
