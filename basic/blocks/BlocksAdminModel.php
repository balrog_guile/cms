<?php
/* =============================================================================
 * ブロック用基礎モデル
 * ========================================================================== */
namespace app\blocks;
use yii\db\QueryBuilder;
use yii\db\Schema;
use Yii;
//use yii\base\Model;

class BlocksAdminModel extends \yii\db\ActiveRecord
{
    // ----------------------------------------------------
    /**
     * ルールを追加
     */
    public function rules()
    {
        $rules = [];
        //$rules['post_id'] = [ ['post_id'],  'safe' ];
        //$rules['version_id'] = [ ['version_id'], 'safe' ];
        //$rules[] = [ ['version_blocks_id'], 'required' ];
        $rules[] = [ ['create_date'], 'required' ];
        $rules[] = [ ['update_date'], 'required' ];
        $rules[] = [ ['user_id'], 'required' ];
        //$rules['is_publish'] = [ ['is_publish'], 'required' ];
        return $rules;
    }
    // ----------------------------------------------------
    /**
     * ラベルを追加
     */
    public function attributeLabels()
    {
        $label = [];
        //$label['post_id'] = Yii::t('app', 'ポストID');
        //$label['version_id'] = Yii::t('app', 'バージョンID');
        //$label['version_blocks_id'] = Yii::t('app', 'ブロックバージョンID');
        $label['create_date'] = Yii::t('app', '作成日');
        $label['update_date'] = Yii::t('app', '更新日');
        $label['user_id'] = Yii::t('app', '更新者ID');
        //$label['is_publish'] = Yii::t('app', '公開状態');
        return $label;
    }
    
    // ----------------------------------------------------
    /**
     * 基本セットを追加
     */
    static public function addBasicColumns( &$define )
    {
        //$define['post_id'] = 'int NOT NULL';
        //$define['version_id'] = 'int NOT NULL';
        //$define['version_blocks_id'] = 'int unsigned NOT NULL';
        $define['create_date'] = 'datetime';
        $define['update_date'] = 'datetime';
        $define['user_id'] = 'int NOT NULL DEFAULT 0 ';
        //$define['is_publish'] = 'int DEFAULT 1';
    }
    
    // ----------------------------------------------------
}