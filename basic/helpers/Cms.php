<?php
/* =============================================================================
 * CMSviewヘルパ
 * ========================================================================== */
namespace app\helpers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;


class Cms
{
    // ----------------------------------------------------
    /**
     * LoadArea
     */
    static public function Area( $name )
    {
        return Yii::$app->runAction('area', ['name'=>$name]);
    }
    
    // ----------------------------------------------------
    /**
     * ブロッククラスの表示データを整形
     * @param object $model BlocksModelの1行文データ
     */
    static public function getBlockData( $model )
    {
        $baseName = preg_replace( '/Blocks$/', '',$model->class_name );
        $set = [
                'dir' => Yii::getAlias('@app') . '/blocks/' . $baseName . '/',
                'url' => \yii\helpers\Url::home(true) . '/blocks/' . $baseName . '/',
                'class' => $model->class_name,
                'classFile' => $model->class_name . '.php',
                'name' => $model->name,
                'desc' => $model->desc,
            ];
        return (object)$set;
    }
    
    // ----------------------------------------------------
}