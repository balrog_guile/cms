<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SiteInfoModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-info-model-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'site_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'domain')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'mobile_mode')->dropDownList([0=>'レスポンシブ', 1=>'ビュー分離']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'is_active')->dropDownList([0=>'非公開',1=>'公開']) ?>
        </div>
    </div>
    
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'active_template')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'active_sp_tenplate')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'サイト登録') : Yii::t('app', 'サイト編集'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
