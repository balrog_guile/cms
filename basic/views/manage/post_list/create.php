<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PostListModel */

$this->title = 'ポストリスト作成';

//パンくず
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'ポストリスト管理'),
    'url' => ['index']
   ];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="post-list-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
