/* =============================================================================
 * サイト管理用JS
 * ========================================================================== */
$(document).ready(function(){
    
    // ----------------------------------------------------
    /**
     * プロパティ
     */
    var util = new ginowanUtil();
    
    
    // ----------------------------------------------------
    /**
     * 初期設定／dialog用
     */
    (function(){
        var dialogArea =
                $('<div></div>')
                .attr('id','dialogArea');
        $('body').append(dialogArea);
        $('#dialogArea').hide();
        $('#dialogArea').dialog({
            'autoOpen': false,
            'title': 'ブロック追加',
            'modal': true
        });
        
    })();
    
    // ----------------------------------------------------
    /**
     * 操作パネル
     */
    (function(){
        if($('#__gnwn_admin_panel_always').length > 0 )
        {
            $('#__gnwn_admin_panel_always').dialog({
                'width': '150px',
                'height': '300',
                'title': '宜野湾操作パネル',
                'dialogClass': '__gnwn_admin_panel_always',
                open:function(event, ui){
                    $('.__gnwn_admin_panel_always').find(".ui-dialog-titlebar-close").hide();
                },
                position: {
                    my: "right center",
                    at: "right center",
                },
                buttons: [
                    {
                        text: '□',
                        click: function() {
                          $('#__gnwn_admin_panel_always').slideDown();
                        }
                    },
                    {
                        text: "_",
                        click: function() {
                          $('#__gnwn_admin_panel_always').slideUp();
                        }
                    }
                ]
            });
        }
    })();
    
    // ----------------------------------------------------
    /**
     * エリアに追加ボタン
     */
    $('.__gnwn_area_add .__gnwn_area_add_exec').on(
        'click',
        function(){
            
            var mode = $(this).attr('data-mode');
            var title, setWidth, setHeight;
            switch( mode )
            {
                //記事追加
                case 'addPost':
                    title = '記事を追加';
                    setWidth = $(window).width() * 0.8;
                    setHeight = $(window).height() * 0.8;
                    break;
                
                //デフォルト
                default:
                    title = 'ブロック追加';
                    mode = 'addBlock';
                    setWidth = $(window).width() * 0.7;
                    setHeight = $(window).height() * 0.7;
                    break;
            }
            
            $('#dialogArea').dialog(
                    'option',
                    {
                        'title': title,
                        'width': setWidth,
                        'height': setHeight
                    }
            );
            $('#dialogArea').empty();
            $('#dialogArea').dialog('open');
            
            //動作を設定
            switch( mode )
            {
                //記事追加
                case 'addPost':
                    addPost( this );
                    break;
                
                //ブロック追加
                case 'addBlock':
                    addBlock( this );
                    break;
                
                //デフォルト
                default:
                    break;
            }
            
        }
    );
    
    // ----------------------------------------------------
    /**
     * 記事情報編集
     */
    $('#__gnwn_admin_panel_always .__gnwn_area_add_exec').on(
        'click',
        function(e){
            
            var mode = $(this).attr('data-mode');
            var title, setWidth, setHeight;
            var dialog = false;
            switch(mode){
                
                //ページ状態編集
                case 'pageInfo':
                    title = 'ページ情報編集';
                    setWidth = $(window).width() * 0.8;
                    setHeight = $(window).height() * 0.8;
                    dialog = true;
                    break;
                
                //エディットモードへ
                case 'go2Edit':
                    go2Edit( this );
                    return;
                    break;
                
                //編集完了確認ダイアログ
                case 'editFinishDialog':
                    title = '編集を終了しますか？';
                    setWidth = $(window).width() * 0.5;
                    setHeight = $(window).height() * 0.5;
                    dialog = true;
                    //dialog_id = '#__gnwn_admin_panel_edit_finish';
                    break;
            }
            
            
            //ダイアログ処理
            if(dialog)
            {
                $('#dialogArea').dialog(
                        'option',
                        {
                            'title': title,
                            'width': setWidth,
                            'height': setHeight
                        }
                );
                $('#dialogArea').empty();
                $('#dialogArea').dialog('open');
            }
            
            
            
            
            //ページ編集モード
            switch(mode){
                
                //ページ状態編集
                case 'pageInfo':
                    addPost(this);
                    break;
                
                //ページ編集終了
                case 'editFinishDialog':
                    displayEditFinishDialog( this )
                    break;
            }
            
            
        }
    );
    
    // ----------------------------------------------------
    /**
     * ページ編集ダイアログの表示
     */
    function displayEditFinishDialog( elem )
    {
        var html = $('#__gnwn_admin_panel_edit_finish').html();
        var post_id = $(elem).attr('data-post-id');
        $('#dialogArea').append(html);
        
        $('#dialogArea')
          .find('.__gnwn_admin_panel_edit_finish_btn_list input[type=button]')
          .on(
            'click',
            function(e){
                var mode = $(this).attr('name');
                var form = $( '#dialogArea .__gnwn_admin_panel_edit_finish_form' );
                $(form).find('input[name=mode]').val(mode);
                $(form).find('input[name=id]').val(post_id);
                $(form).find('input[name=url]').val(location.href);
                $(form).submit();
            }
          );
        
    }
    
    // ----------------------------------------------------
    /**
     * 編集モードの出入り
     */
    function go2Edit( elem )
    {
        var params = {
            'url': location.href,
            'go': 'edit',
            'post_list_id': $(elem).attr('data-post_list_id'),
            'id': $(elem).attr('data-post-id')
        };
        var url = urlList['goEdit'] + '?';
        var urlparams = [];
        for( var key in params )
        {
            urlparams.push( key + '=' + encodeURIComponent(params[key]) );
        }
        url += urlparams.join( '&' );
        location.href = url;
    }
    // ----------------------------------------------------
    /**
     * ブロック追加
     * @param {type} elem
     * @returns {undefined}
     */
    function addBlock( elem )
    {
        var url = urlList['blockList'];
        //$('#dialogArea').append(url);
        var params = {
            'areaName': $(elem).attr('date-areaname')
        };
        
        var callback = function( data ){
            $('#dialogArea').append(data);
            
            $('#dialogArea').find('.gnwn_listPanel_editBlock').on(
                    'click',
                    function(e){
                        var blockInstallId = $(this).attr('data-blockInstallId');
                        var width = $(this).attr('data-blockPanelWidth');
                        var height = $(this).attr('data-blockPanelHeight');
                        var title =  $(this).attr('data-blockName');
                        var areaName =  $(this).attr('data-area-name');
                        
                        //パネル表示
                        blockEditPanel(
                            title,
                            width,
                            height,
                            blockInstallId,
                            areaName
                        );
                    }
            );
            
        }
        
        util.get( url, params,'html', callback );
    }
    
    // ----------------------------------------------------
    /**
     * ブロック編集パネル
     */
    function blockEditPanel(
        title, width, height, blockInstallID,
        areaName, blockId
    )
    {
        var editingVersionId = postData.editingVersionId;
        var postId = postData.id;
        var url = urlList.blockEdit;
        if( blockId == undefined ){ blockId = null; }
        
        //パネル表示
        var panel = $('<div></div>').attr( 'id', 'blockEditPanel' );
        $('body').append( panel );
        $('#blockEditPanel').hide();
        $('#blockEditPanel').dialog({
            'width': $(window).width() * ( width /100 ),
            'height': $(window).height() * ( height /100 ),
            'title': title,
            'modal': true,
            'close': function(){
                $('#blockEditPanel').remove();
            }
        });
        
        
        //編集画面を表示
        var params = {
            'editingVersionId': editingVersionId,
            'blockId': blockId,
            'postId': postId,
            'areaName': areaName
        };
        var callBack = function(data){
            //成功
            if( data == '{{GINOWANCODE::::BLOCK_EDIT_OK}}')
            {
                $('#blockEditPanel').dialog('close');
                $('#dialogArea').dialog('close');
                location.reload();
                return;
            }
            $('#blockEditPanel').append(data);
            $('#blockEditPanel .gnwn__edit_block_now form').on(
                'submit',
                function(e){
                    var url = $(this).attr('action');
                    var params = $(this).serialize();
                    $('#blockEditPanel').empty();
                     util.post(
                        url,
                        params,
                        'html',
                        callBack
                    );
                    return false;
                }
            );
        };
        //alert(url);
        util.get(
            url,
            params,
            'html',
            callBack
        );
        
    }
    
    // ----------------------------------------------------
    /**
     * 記事/編集追加モード
     */
    function addPost( elem )
    {
        var url = urlList['addPost'];
        var params = {
            'post_list_id': $(elem).attr('data-post_list_id')
        };
        var id = $(elem).attr('data-post-id');
        if( id != undefined ){ params['id'] = id; }
        
        var type = 'get';
        
        //パネルの初期化
        var resetPanelSubmit = function(){
            $('#dialogArea').find('.__gnwn_panel_post_send').on(
                'submit',
                function(e){
                    var forms = $(this).serializeArray();
                    var url = $(this).attr('action');
                    var success = function( ret ){
                        $('#dialogArea').empty().append(ret);
                        resetPanelSubmit();
                    };
                    util.post( url, forms, 'html',success );
                    return false;
                }
            );
        }
        
        $.ajax({
            'url': url,
            'data': params,
            'dataType': 'html',
            'type': type,
            'error': function(e){
                alert('通信エラー');
            },
            'success': function(data){
                $('#dialogArea').append(data);
                resetPanelSubmit();
            }
        });
    }
    
    // ----------------------------------------------------
    
});