<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_version".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $version_num
 * @property string $name
 * @property string $create_date
 * @property string $update_date
 * @property integer $is_publish
 * @property integer $user_id
 * @property integer $is_edit_now
 */
class PostVersionModel extends \yii\db\ActiveRecord
{
    // ----------------------------------------------------
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_version';
    }
    
    // ----------------------------------------------------
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'create_date', 'update_date', 'user_id'], 'required'],
            [['post_id', 'version_num', 'is_publish', 'user_id', 'is_edit_now'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }
    
    // ----------------------------------------------------
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_id' => Yii::t('app', 'ポストのID'),
            'version_num' => Yii::t('app', 'バージョンナンバー'),
            'name' => Yii::t('app', 'バージョン名'),
            'create_date' => Yii::t('app', '作成日'),
            'update_date' => Yii::t('app', '更新日'),
            'is_publish' => Yii::t('app', '公開バージョンに1をつける'),
            'user_id' => Yii::t('app', 'このバージョンを記録したユーザーID'),
            'is_edit_now' => Yii::t('app', 'Is Edit Now'),
        ];
    }
    
    
    // ----------------------------------------------------
}
