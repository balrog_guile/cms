<?php
use app\models\VersionBlocksModel;
?>

<?php foreach( VersionBlocksModel::getAreaDataList( $versionId, $name ) as $model ): ?>
    <?= $model->blocks->name; ?>
<?php endforeach; ?>
