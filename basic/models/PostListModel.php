<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_list".
 *
 * @property integer $id
 * @property integer $site_info_id
 * @property string $key
 * @property string $name
 * @property string $create_date
 * @property string $update_date
 * @property integer $open_status
 * @property integer $delete_flag
 * @property string $base_url
 */
class PostListModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_info_id', 'key', 'name', 'create_date', 'update_date'], 'required'],
            [['site_info_id', 'open_status', 'delete_flag'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['key', 'name', 'base_url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'site_info_id' => Yii::t('app', 'サイト名'),
            'key' => Yii::t('app', 'ポストリストキー'),
            'name' => Yii::t('app', 'ポストリストの名前'),
            'create_date' => Yii::t('app', 'Create Date'),
            'update_date' => Yii::t('app', 'Update Date'),
            'open_status' => Yii::t('app', '公開フラグ'),
            'delete_flag' => Yii::t('app', '削除フラグ'),
            'base_url' => Yii::t('app', 'ベースURL'),
        ];
    }
}
