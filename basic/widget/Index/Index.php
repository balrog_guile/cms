<?php
/* =============================================================================
 * インデックス
 * ========================================================================== */
namespace app\widget\Index;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\base\Widget;

use app\models\PostSearchModel;

class Index extends Widget
{
    //ポストリストキー
    public $key = null;
    
    //ポストリスト
    public $postList = null;
    
    //表示記事数
    public $pageMax = 5;
    
    //データプロバイダ
    public $dataProvider = null;
    
    // ----------------------------------------------------
    /**
     * 共通処理
     */
    public function commonSet( &$data )
    {
        $data['postList'] = $this->postList;
        $data['key'] = $this->key;
        
    }
    // ----------------------------------------------------
    
    public function init()
    {
        parent::init();
        $data = array();
        
        //対象のポストリスト
        if(is_null( $this->key ))
        {
            $this->postList = Yii::$app->controller->postList;
        }
        else
        {
             $postList = \app\models\PostListModel::find()
                ->where( 'base_url = :base_url', [':base_url' => $this->indexDirectory ] )
                ->one();
            if(! is_null($postList) )
            {
                $this->postList = $postList;
            }
        }
        
        
        //対象記事のデータをアクティブデータプロバイダとして取得
        if( $this->postList !== NULL )
        {
            $PostSearchModel = new PostSearchModel();
            //$PostSearchModel->widgetPageMax = $this->pageMax;
            $PostSearchModel->widgetPageMax = 1;
            $searchParams = [
                'PostSearchModel' => [
                    'post_list_id' => $this->postList->id,
                    'is_index' => 0
                ]
            ];
            $this->dataProvider =
                    $PostSearchModel->PostWidget( $searchParams );
        }
        
        $this->commonSet($data);
    }
    
    // ----------------------------------------------------
    
    public function run()
    {
        $data = array();
        $this->commonSet($data);
        if(!is_null(Yii::$app->user->id))
        {
            echo $this->render('addArea', $data );
        }
    }
    // ----------------------------------------------------
}