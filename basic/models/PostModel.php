<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property integer $post_list_id
 * @property string $subject
 * @property string $slug
 * @property string $create_date
 * @property string $update_date
 * @property string $display_date
 * @property string $open_date
 * @property string $close_date
 * @property string $meta_key
 * @property string $meta_desc
 * @property integer $open_status
 * @property integer $delete_flag
 * @property integer $index_display
 */
class PostModel extends \yii\db\ActiveRecord
{
    // ----------------------------------------------------
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }
    
    // ----------------------------------------------------
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'post_list_id', 'subject', 'slug', 'create_date', 'update_date', 'display_date'], 'required'],
            [['id', 'post_list_id', 'open_status', 'delete_flag', 'index_display', 'is_index'], 'integer'],
            [['create_date', 'update_date', 'display_date', 'open_date', 'close_date'], 'safe'],
            [['meta_key', 'meta_desc'], 'string'],
            [['subject', 'slug'], 'string', 'max' => 255]
        ];
    }
    
    // ----------------------------------------------------
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_list_id' => Yii::t('app', 'ポストリストID'),
            'subject' => Yii::t('app', 'タイトル'),
            'slug' => Yii::t('app', 'URL用スラグ'),
            'create_date' => Yii::t('app', '作成日'),
            'update_date' => Yii::t('app', '更新日'),
            'display_date' => Yii::t('app', '表示作成日'),
            'open_date' => Yii::t('app', '公開日'),
            'close_date' => Yii::t('app', '公開終了日'),
            'meta_key' => Yii::t('app', 'metaタグキーワード'),
            'meta_desc' => Yii::t('app', 'metaタグ詳細説明'),
            'open_status' => Yii::t('app', '公開ステータス
0: 非公開
1: 公開'),
            'delete_flag' => Yii::t('app', '削除フラグ
1: ゴミ箱'),
            'index_display' => Yii::t('app', '一覧への表示
0: 一覧出力なし
1: 一覧出力あり'),
            'is_index' => Yii::t( 'app', 'インデックスか' )
        ];
    }
    
    // ----------------------------------------------------
    /**
     * 現在公開バージョン
     */
    public function getCurrentVersion()
    {
        return $this->hasOne(
                    PostVersionModel::className(),
                    [
                        'post_id' => 'id'
                    ]
                )
                ->where(
                    'is_publish = :is_publish',
                    [
                        ':is_publish' => 1
                    ]
                )
        ;
    }
    
    // ----------------------------------------------------
    /**
     * 最新編集バージョン
     */
    public function getEditingVersion()
    {
        return $this->hasOne(
                    PostVersionModel::className(),
                    [
                        'post_id' => 'id'
                    ]
                )
                ->orderBy( 'version_num DESC' )
                ->limit( 1 );
        ;
    }
    
    // ----------------------------------------------------
    /**
     * 編集中状態
     */
    public function getEditNow()
    {
        return $this->hasOne(
                    PostVersionModel::className(),
                    [
                        'post_id' => 'id'
                    ]
                )
                ->where(
                    'is_edit_now = :is_edit_now',
                    [
                        ':is_edit_now' => 1
                    ]
                )
        ;
    }
    
    // ----------------------------------------------------
    
}
