<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blocks".
 *
 * @property integer $id
 * @property string $name
 * @property string $width
 * @property string $height
 * @property string $create_date
 * @property string $update_date
 * @property integer $rank
 * @property integer $icon_id
 */
class BlocksModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'create_date', 'update_date', 'model_name_space', 'model_class'], 'required'],
            [['create_date', 'update_date', 'desc', 'name_sapace', 'class_name' ], 'safe'],
            [['rank', 'icon_id'], 'integer'],
            [['name', 'width', 'height'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'ブロック名'),
            'desc' => Yii::t('app', 'ブロックの説明'),
            'width' => Yii::t('app', '編集ウインドウサイズ（幅）'),
            'height' => Yii::t('app', '編集ウインドウサイズ（高さ）'),
            'create_date' => Yii::t('app', 'Create Date'),
            'update_date' => Yii::t('app', 'Update Date'),
            'rank' => Yii::t('app', '並び順'),
            'icon_id' => Yii::t('app', 'アイコンのID'),
            'name_sapace' => Yii::t('app', '名前空間'),
            'class_name' => Yii::t('app', 'クラス名'),
            'model_name_space' => Yii::t('app', 'モデル名前空間'),
            'model_class' => Yii::t('app', 'モデルクラス名'),
        ];
    }
}
