<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PostListModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-list-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'base_url')->textInput(['maxlength' => true]) ?>
    <?php
       $site_info = [];
       foreach( \app\models\SiteInfoModel::find()->all() as $row ){
           $site_info[ $row->id ] = $row->site_name;
       }
       echo $form->field($model, 'site_info_id')
            ->dropDownList($site_info);
    ?>
    <?=
       $form->field($model, 'open_status')
            ->dropDownList(
                [
                    '0' => '非公開',
                    '1' => '公開'
                ],
                [
                    
                ]
            );
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'ポストリスト作成') : Yii::t('app', '編集'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
