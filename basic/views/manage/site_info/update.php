<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SiteInfoModel */

$this->title = Yii::t('app', 'サイト情報編集');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'サイト情報'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', '編集');
?>
<div class="site-info-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<hr />
<h3>ポストリスト</h3>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>公開状態</th>
            <th>スラグ</th>
            <th>名前</th>
            <th>ベースURL</th>
            <th>詳細</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach( $model->postList as $postList ): ?>
        <tr>
            <td><?= $postList->id; ?></td>
            <td><?php if( $postList->open_status == 1):?>公開<?php else: ?>非公開<?php endif; ?></td>
            <td><?= $postList->key; ?></td>
            <td><?= $postList->name; ?></td>
            <td><?= $postList->base_url; ?></td>
            <td>詳細</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>