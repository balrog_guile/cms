<div id="__gnwn_admin_panel">
</div>
<div id="__gnwn_admin_panel_always">
    <?php if( ($self->editing->edit_now)&&($self->editing->edit_user ) ): ?>
        <ul>
            <li>
                <a
                    href="javascript:void(0);"
                    data-post-id="<?= $self->post->id ?>"
                    data-post_list_id="<?= $self->post->post_list_id ?>"
                    class="__gnwn_area_add_exec"
                    data-mode="editFinishDialog"
                >
                    編集完了
                </a>
            </li>
        </ul>
    <?php elseif( ($self->editing->edit_now)&&($self->editing->edit_user === false) ): ?>
        <ul>
            <li>他人がエディット中</li>
        </ul>
    <?php else: ?>
        <ul>
            <li>
                <a
                    href="javascript:void(0);"
                    data-post-id="<?= $self->post->id ?>"
                    data-post_list_id="<?= $self->post->post_list_id ?>"
                    class="__gnwn_area_add_exec"
                    data-mode="go2Edit"
                >
                    編集モードへ
                </a>
            </li>
            <li>
                <a
                    href="javascript:void(0);"
                    data-post-id="<?= $self->post->id ?>"
                    data-post_list_id="<?= $self->post->post_list_id ?>"
                    class="__gnwn_area_add_exec"
                    data-mode="pageInfo"
                >
                    ページ情報へ
                </a>
            </li>
            <li><a href="javascript:void(0);">スマートフォンモードへ</a></li>
        </ul>
    <?php endif; ?>
    <hr />
    <ul>
        <li>
            <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl( 'manage/panel' ); ?>">
                管理パネルへ
            </a>
        </li>
    </ul>
</div>

<!-- 編集終了ダイアログ -->
<div id="__gnwn_admin_panel_edit_finish" style="display: none;">
    <div class="__gnwn_admin_panel_edit_finish_btn_list" style="text-align: center; font-size: 2.5em;">
        <input type="button" name="poi" value="この編集を破棄" />
        <input type="button" name="draft" value="この編集を下書き保存" />
        <input type="button" name="open" value="この編集を公開" />
    </div>
    <form
        action="<?php echo Yii::$app->urlManager->createAbsoluteUrl('manage/api/site_manage/finish_edit'); ?>"
        method="post"
        class="__gnwn_admin_panel_edit_finish_form"
    >
        <input type="hidden" name="mode" value="" />
        <input type="hidden" name="id" value="" />
        <input type="hidden" name="url" value="" />
    </form>
</div>
<!-- 編集終了ダイアログ -->