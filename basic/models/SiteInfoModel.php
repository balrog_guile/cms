<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_info".
 *
 * @property integer $id
 * @property string $site_name
 * @property string $domain
 * @property string $active_template
 * @property string $active_sp_tenplate
 * @property integer $mobile_mode
 * @property integer $is_active
 */
class SiteInfoModel extends \yii\db\ActiveRecord
{
    // ----------------------------------------------------
    /**
     * ポストリストリレーション
     */
    public function getPostList()
    {
        return $this->hasMany(
                PostListModel::className(),
                ['site_info_id' => 'id']
        );
    }
    
    // ----------------------------------------------------
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_name', 'domain', 'is_active','active_template'], 'required'],
            [['mobile_mode', 'is_active'], 'integer'],
            [['site_name', 'domain', 'active_template', 'active_sp_tenplate'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'site_name' => Yii::t('app', 'サイト名'),
            'domain' => Yii::t('app', 'このサイト情報を利用するドメイン'),
            'active_template' => Yii::t('app', 'PCテンプレート'),
            'active_sp_tenplate' => Yii::t('app', 'SPテンプレート'),
            'mobile_mode' => Yii::t('app', 'モバイルテンプレート処理'),
            'is_active' => Yii::t('app', '公開状態'),
        ];
    }
}
