<?php
/* =============================================================================
 * ページ処理用
 * ========================================================================== */
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;

use app\models\UserModel;
use \app\models\PostModel;
use \app\models\PostVersionModel;
use app\assets\PagesAsset;

use app\helpers\Cms;



class SiteController extends Controller
{
    // ----------------------------------------------------
    /**
     * プロパティ処理
     */
    
    //テンプレートセット名
    public $tempale_name = 'default';
    
    //assetFolder
    static public $asset = 'site';
    
    //assetpth
    public $assetPath = '';
    
    //ビュー用のデータ
    public $viewData = [];
    
    //URLセグメント
    public $segmentList = [];
    
    //インデックスページの場合ディレクトリ（ポストリスト判定用）
    public $indexDirectory = null;
    
    //このディレクトリに割り振られているポストリスト
    public $postList = null;
    
    //テンプレートパス
    public $templatePath = [];
    
    //セグメントリストからとったファイル名
    public $fileName = null;
    
    //インデックスページか
    public $isIndex = false;
    
    //処理済みパスを保存
    public $pathInfomation = [];
    
    //詳細ページポストの内容
    public $post = null;
    
    //エディット状態などを保存
    public $editing = [];
    
    // ----------------------------------------------------
    public function behaviors()
    {
        return [
        ];
    }
    
    // ----------------------------------------------------
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    // ----------------------------------------------------
    /**
     * 初期処理
     */
    public function init() {
        parent::init();
        
        //テンプレートセット
        $this->tempale_name = 'default';
        
        //アセットフォルダ
        $this->assetPath = Yii::getAlias('@app') . '/pagesAsset';
        
        //URIセグメント
        
    }
    
    // ----------------------------------------------------
    /**
     * パス処理
     */
    protected function setPath( $path )
    {
        $isIndex = false;
        if( preg_match('/\/$/', $path) )
        {
            $path .= 'index.html';
            $isIndex = true;
        }
        else if(! preg_match('/\.html$/', $path) )
        {
            $path .= '/index.html';
            $isIndex = true;
        }
        $this->isIndex = $isIndex;
        
        
        $path = preg_replace( '/\.html$/', '.php', $path );
        
        
        //セグメントをとっておく
        $this->segmentList = explode( '/', $path );
        
        
        
        //インデックスの場合フォルダをとっておく
        $list = $this->segmentList;
        $fileName = array_pop($list);
        $this->indexDirectory = implode( '/', $list ) . '/';
        $this->fileName = $fileName;
        
        
        //各種パス解決
        $filePath = Yii::getAlias('@webroot') . '/templates/' . $this->tempale_name . '/';
        $filePath .= $path;
        
        
        
        
        $return = [
            'filePath' => $filePath,
            'isIndex' => $isIndex,
            'basePath' => Yii::$app->basePath,
            'baseUrl' => Url::home(true),
            'templatePath' => Yii::getAlias('@webroot') . '/templates/' . $this->tempale_name .'/',
            'templateUrl' => Url::home(true) . 'templates/' . $this->tempale_name .'/',
            'indexDirectory' => $this->indexDirectory,
            'templatePath' => Yii::getAlias('@webroot') . '/templates/' . $this->tempale_name . '/',
            'templateUrl' => Url::home(true) . 'templates/' . $this->tempale_name .'/'
            
        ];
        $this->pathInfomation = $return;
        
        
        return $return;
    }
    
    // ----------------------------------------------------
    /**
     * 通常ページ処理用
     * @param type $path
     * @return type 
     */
    
    
    public function actionIndex( $path = '' )
    {
        //ビュー用データ
        $data = array();
        
        
        //パス処理
        $pathDatas = $this->setPath($path);
        $filePath = $pathDatas['filePath'];
        
        
        //ポストリスト取得
        $this->getPostList();
        
        
        //インデックス処理
        if(
            ($this->isIndex === true)&&
            ($this->postList!==NULL)
        )
        {
            //インデックス処理
            $indexPost = PostModel::find()
                        ->where(
                            'post_list_id = :post_list_id AND is_index = 1',
                            [ ':post_list_id' => $this->postList->id ]
                        )
                        ->one();
            if(is_null($indexPost))
            {
                //ポスト作成
                $indexPost = new PostModel();
                $indexPost->post_list_id = $this->postList->id;
                $indexPost->subject = 'インデックス';
                $indexPost->slug = 'index';
                $indexPost->create_date = date('Y-m-d H:i:s');
                $indexPost->update_date = date('Y-m-d H:i:s');
                $indexPost->display_date = date('Y-m-d H:i:s');
                $indexPost->meta_key = '';
                $indexPost->meta_desc = '';
                $indexPost->open_status = 1;
                $indexPost->delete_flag = 0;
                $indexPost->index_display = 0;
                $indexPost->is_index = 1;
                $indexPost->save();
                
                //最初のバージョン生成
                $PostVersionModel = new PostVersionModel();
                $PostVersionModel->post_id = $indexPost->id;
                $PostVersionModel->version_num = 1;
                $PostVersionModel->name = '初期バージョン';
                $PostVersionModel->create_date = date('Y-m-d H:i:s');
                $PostVersionModel->update_date = date('Y-m-d H:i:s');
                $PostVersionModel->is_publish = 1;
                $PostVersionModel->user_id = Yii::$app->user->id;
                $PostVersionModel->save();
                
                
                
            }
            $data['post'] = $indexPost;
            $this->post = $indexPost;
            
            
            
            
            //共通処理
            $this->commonDatas($data);
            
            return $this->renderFile( $filePath, $data );
            
        }
        
        
        //ファイルがある場合
        $getDetail = $this->getDetail();//ファイル指定でポストをとるために先に投稿をチェック
        $spicityFile = $this->pathInfomation['templatePath'] . $this->indexDirectory . $this->fileName;
        if(file_exists($spicityFile))
        {
            $data['post'] = $this->post;
            //共通処理
            $this->commonDatas($data);
            return $this->renderFile( $spicityFile, $data );
        }
        
        
        //ポストなら
        if(
            (!is_null($this->postList))&&
            (!is_null($getDetail))
        )
        {
            $filePath = 
                    $this->pathInfomation['templatePath'] .
                    $this->indexDirectory . 'detail.php';
            
            $data['post'] = $this->post;
            
            //共通処理
            $this->commonDatas($data);
            
            return $this->renderFile( $filePath, $data );
        }
        
        
        //////TODO その他の機能を挟むフックをここに
        
        
        //なにもなければ404
        throw new NotFoundHttpException();
    }
    
    // ----------------------------------------------------
    
    /**
     * 詳細投稿
     */
    public function getDetail()
    {
        //なければ無視
        if( $this->postList === NULL)
        {
            return null;
        }
        
        //スラグの処理
        $pathInfo = pathinfo($this->fileName);
        $fileName = $pathInfo['filename'];
        
        //データ取得
        $this->post =
                PostModel::find()
                    ->where(
                        'slug = :slug AND post_list_id = :post_list_id',
                        [
                            ':slug' => $fileName,
                            ':post_list_id' => $this->postList->id
                            
                        ]
                    )
                    ->one()
                ;
        
        return $this->post;
    }
    
    // ----------------------------------------------------
    
    /**
     * ポストリストを取得
     */
    protected function getPostList()
    {
        $postList = \app\models\PostListModel::find()
                ->where( 'base_url = :base_url', [':base_url' => $this->indexDirectory ] )
                ->one();
        if(! is_null($postList) )
        {
            $this->postList = $postList;
        }
    }
    
    // ----------------------------------------------------
    
    /**
     * データの共通処理
     * @param (ref)array $data
     */
    protected function commonDatas( &$data )
    {
        //パス処理
        $data['basePath'] = $this->pathInfomation['basePath'];
        $data['baseUrl'] = $this->pathInfomation['baseUrl'];
        $data['templatePath'] = $this->pathInfomation['templatePath'];
        $data['templateUrl'] = $this->pathInfomation['templateUrl'];
        $data['isIndex'] = $this->isIndex;
        
        //ログイン中か
        if(is_null(Yii::$app->user->id))
        {
            $data['login'] = false;
            $data['user_data'] = null;
        }
        else
        {
            $data['login'] = true;
            $user = UserModel::findOne(Yii::$app->user->id);
            $data['user_data'] = $user;
        }
        
        
        //エディット状態などを保存
        $this->editing = new \stdClass();
        if(
            (is_null($this->post))||
            (is_null($this->post->editNow) )
        )
        {
            $this->editing->edit_now = FALSE;
            $this->editing->edit_user = null;
        }
        else
        {
            $this->editing->edit_now = true;
            if( $this->post->editNow->user_id == Yii::$app->user->id )
            {
                $this->editing->edit_user = true;
            }
            else{
                $this->editing->edit_user = false;
            }
        }
        
        
        
        //このオブジェクト自体
        $data['self'] = $this;
        
        
        //assetPath
        $data['assetPathData'] = Yii::$app->assetManager->publish(
                $this->assetPath,
                [
                    'forceCopy' => true,
                ]
        );
        $data['assetUrl'] = Url::home(true) . $data['assetPathData'][1] . '/';
        
        
        //アセット追加
        $data['jsList'] = array();
        $data['cssList'] = array();
        $data['jsRawList'] = array();
        if( $data['login'] )
        {
            //JS
            $data['jsList'] = array(
                'js/jquery.js',
                'js/jquery-ui.min.js',
                'js/siteAdminUtility.js',
                'js/siteAdmin.js',
            );
            foreach($data['jsList'] as $js)
            {
                Yii::$app->view->registerJsFile($js);
            }
            
            //css
            $data['cssList'] = array(
                'css/jquery-ui.min.css',
                'css/jquery-ui.structure.min.css',
                'css/jquery-ui.theme.min.css',
                'css/ginowan.css',
            );
            foreach( $data['cssList'] as $css )
            {
                Yii::$app->view->registerCssFile( $css );
            }
            
            //JSデータ
            $jsRawUrl = [
                'addPost' => Yii::$app->urlManager->createAbsoluteUrl('manage/api/site_manage/add_post'),
                'goEdit' => Yii::$app->urlManager->createAbsoluteUrl('manage/api/site_manage/go_edit'),
                'blockList' => Yii::$app->urlManager->createAbsoluteUrl('manage/api/blocks'),
                'blockEdit' => Yii::$app->urlManager->createAbsoluteUrl('manage/api/blocks/edit'),
            ];
            if( isset($this->post) && !is_null($this->post))
            {
                $postData = [
                    'id' => $this->id,
                    'currentVersion' => $this->post->currentVersion->id,
                    'editingVersionId' => $this->post->editingVersion->id,
                ];
            }
            else{
                $postData = [
                    'id' => 0,
                    'currentVersion' => 0,
                    'editingVersionId' => 0,
                ];
            }
            $data['jsRawList'] = array(
                'var urlList = ' . json_encode($jsRawUrl) . ';',
                'var postData = ' . json_encode($postData) . ';'
            );
            
        }
        
        
        $this->viewData =& $data;
    }
    
    // ----------------------------------------------------
}
