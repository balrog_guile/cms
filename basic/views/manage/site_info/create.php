<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SiteInfoModel */

$this->title = Yii::t('app', 'サイト情報作成');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'サイト管理'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-info-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
