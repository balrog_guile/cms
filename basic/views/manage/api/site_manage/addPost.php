<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php
    $form = ActiveForm::begin([
        'errorCssClass' => '__gnwn_panel_error',
        'options' => [
            'class' => '__gnwn_panel_post_send',
        ]
    ]);
?>
    
    <?= $form->field($model, 'subject')->textInput(
        [
            'maxlength' => true,
            'style' => 'width: 100%; font-size: 1.5em;'
        ])
    ?>
    
    <?= $form->field($model, 'slug')->textInput(
        [
            'maxlength' => true,
            'style' => 'width: 100%; font-size: 1.5em;'
        ])
    ?>
    
    <?= $form->field($model, 'meta_key')->textInput(
        [
            'maxlength' => true,
            'style' => 'width: 100%; font-size: 1.5em;'
        ])
    ?>
    
    <?= $form->field($model, 'display_date')->textInput(
        [
            'maxlength' => true,
            'style' => 'width: 100%; font-size: 1.5em;'
        ])
    ?>
    
    
    <?= $form->field($model, 'open_date')->textInput(
        [
            'maxlength' => true,
            'style' => 'width: 100%; font-size: 1.5em;'
        ])
    ?>
    
    
    <?= $form->field($model, 'close_date')->textInput(
        [
            'maxlength' => true,
            'style' => 'width: 100%; font-size: 1.5em;'
        ])
    ?>
    
    
    <?= $form->field($model, 'open_status')->dropDownList(
        [
            0 => '非公開',
            1 => '公開'
        ],
        [
            'style' => 'width: 100%; font-size: 1.5em;'
        ])
    ?>
    
    <?= $form->field($model, 'index_display')->dropDownList(
        [
            0 => '一覧出力なし',
            1 => '一覧出力あり'
        ],
        [
            'style' => 'width: 100%; font-size: 1.5em;'
        ])
    ?>
    
    <hr />
    
    <?= Html::submitButton(
        $model->isNewRecord ? Yii::t('app', '記事追加') : Yii::t('app', '記事データ編集'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','style' => 'width:100%;']
    )?>
<?php ActiveForm::end(); ?>
